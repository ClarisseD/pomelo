# Depot git

## Repartir sur un nouveau PC

* Creer le projet java vide
* Dans le repertoire du projet ouvrir cmd
  * git init
  * git remote add origin https://... (nom du dépot)
  * git pull origin master
  * 
  

## Enregistrer les modifs sur le git

* Ouvrir le terminal 
* git status
* git add <chemin fichier modif>
* git status 
* git commit -m "modif"
* git pull origin master (pour enregistrer les modifs faites par l'autre)
* git push origin master (entrer user mdp)

