package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.util.List;

import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;
import users.CreationPersonne;
import bdd.Etablissement;
import communication.Message;

/**
 * Classe LoginCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class LoginCommand extends Command {

	/**
	 * Constructeur de LoginCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public LoginCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}
	
	/**
	 * Surcharge de la methode getHelpMessage
	 */
	@Override
	public String getHelpMessage(int type) {
		return super.getHelpMessage(type)
				+ "login --user=PNom --password=NPrenomId" + "\n";
	}

	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement etablissement) throws SQLException {
		String nomUtilisateur = getParameter("user").getValue(args);
		String motDePasse = getParameter("pwd").getValue(args);
		
		if (nomUtilisateur == null || motDePasse == null) {
			System.out.println("La commande 'login' est mal ecrite");
			System.out.println(this.toString());
			return null;
		}
		
		Utilisateur user = new Utilisateur(nomUtilisateur, motDePasse);
		int type = user.connexion();
		
		if (type == 1) { // il s'agit d'un professeur 
			CreationPersonne personneCree = new CreationPersonne(nomUtilisateur, motDePasse);
			Professeur professeur = personneCree.creationProfesseur();
			return professeur;
		}
		
		if (type == 2) { // il s'agit d'un etudiant 
			CreationPersonne personneCree = new CreationPersonne(nomUtilisateur, motDePasse);
			Etudiant etudiant = personneCree.creationEtudiant(etablissement);
			return etudiant;
		}
		
		if (type == 3) { // il s'agit d'un personnel administratif
			CreationPersonne personneCree = new CreationPersonne(nomUtilisateur, motDePasse);
			PersonnelAdministratif personnelAdministratif = personneCree.creationPersonnelAdministratif();
			return personnelAdministratif;
		}
		return user;
	}
	
	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) throws SQLException {
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
	}

	/**
	 * Executable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException {
	}

}
