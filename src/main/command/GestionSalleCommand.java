package main.command;

/**
 * Importations
 */
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import bdd.Etablissement;
import communication.Message;
import main.Main;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe GestionSalleCommand qui herite de Command
 * @author formation
 *
 */
public class GestionSalleCommand extends Command{

	/**
	 * Constructeur de GestionSalleCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public GestionSalleCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}

	/**
	 * Surcharge de la methode getHelpMessage
	 */
	@Override
	public String getHelpMessage(int type) {
		if (type == 3) {
			return super.getHelpMessage(type)
					+ "gestionSalle --modifier=capacite || --modifier=type || --modifier=etat || --modifier=equipement";
		}
		return null;
	}

	/**
	 * Excecutable pour un personnel administratif
	 */
	@Override
	public void exec(String[] args, int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException, ParseException {
		String modifier = getParameter("modifier").getValue(args);

		if (modifier.equals("capacite")) {
			int modif = 1;
			System.out.println("Quelle salle voulez vous modifier");
			int numeroSalle = Main.getScanner().nextInt();
			Main.getScanner().nextLine();
			personnelAdministratif.gestionSalle(numeroSalle, modif, etablissement);
		}

		else if (modifier.equals("type")) {
			int modif = 2;
			System.out.println("Quelle salle voulez vous modifier");
			int numeroSalle = Main.getScanner().nextInt();
			Main.getScanner().nextLine();
			personnelAdministratif.gestionSalle(numeroSalle, modif, etablissement);
		}

		else if (modifier.equals("etat")) {
			int modif = 3;
			System.out.println("Quelle salle voulez vous modifier");
			int numeroSalle = Main.getScanner().nextInt();
			Main.getScanner().nextLine();
			personnelAdministratif.gestionSalle(numeroSalle, modif, etablissement);
		}

		else if (modifier.equals("equipement")) {
			int modif = 4;
			System.out.println("Quelle salle voulez vous modifier");
			int numeroSalle = Main.getScanner().nextInt();
			Main.getScanner().nextLine();
			personnelAdministratif.gestionSalle(numeroSalle, modif, etablissement);
		}
		
		else {
			System.out.println("La commande 'gestionSalle' est mal ecrite");
		}
	}

	/**
	 * Executable pour un professeur
	 */
	@Override
	public void exec(String[] args, int type, Professeur professeur, Etablissement etablissement, List<Message> messages) throws SQLException {
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args, int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
	}
	
	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement etablissement) throws SQLException {
		return null;
	}
}
