package main.command;

/**
 * Importations
 */
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import bdd.Etablissement;
import bulletin.Bulletin;
import communication.Message;
import main.command.model.Command;
import main.command.model.Parameter;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe BulletinCommand qui herite de Command
 * @author Bouillet Ducatillion
 *
 */
public class BulletinCommand extends Command{

	/**
	 * Constructeur de BulletinCommand
	 * @param name
	 * @param helpMessage
	 * @param allowedParameter
	 */
	public BulletinCommand(String name, String helpMessage, List<Parameter> allowedParameter) {
		super(name, helpMessage, allowedParameter);
	}
	
	/**
	 * Surcharge de la methode getHelpMessage
	 */
	@Override
	public String getHelpMessage(int type) {
		if (type == 1){
			return super.getHelpMessage(type)
					+ "bulletin --saisir=note || --saisir=appreciation" + "\n";
		}
		else if (type == 2){
			return super.getHelpMessage(type)
					+ "bulletin --visualiser" + "\n";
		}
		else if (type == 3){
			return super.getHelpMessage(type)
				+ "bulletin --imprimer" + "\n";
		}
		return super.getHelpMessage(type)
				+ "bulletin --imprimer || --visualiser || --saisir=note || --saisir=appreciation" + "\n";
	}

	/**
	 * Executable pour le professeur
	 */
	@Override
	public void exec(String[] args,int type, Professeur professeur, Etablissement eta, List<Message> messages) throws SQLException {
		String saisir = getParameter("saisir").getValue(args);
		
		if (saisir.equals("note")) {
			professeur.saisirNote(eta);
			return;
		}
		
		else if (saisir.equals("appreciation")) {
			professeur.saisirAppreciation(eta);
			return;
		}
		
		else {
			System.out.println("La commande 'saisir' est mal ecrite");
		}
	}

	/**
	 * Executable pour un etudiant
	 */
	@Override
	public void exec(String[] args,int type, Etudiant etudiant, Etablissement etablissement, List<Message> messages) throws SQLException {
		boolean cmdToDisplayVisualiser = getParameter("visualiser").isPresent(args);
		
		if (cmdToDisplayVisualiser) {
			etudiant = Bulletin.creationBulletin(etudiant);
			etudiant.visualiserBulletin();
			return;
		}
		else {
			System.out.println("La commande 'visualiser' est mal ecrite");
		}
	}

	/**
	 * Executable pour un personnel administratif
	 * @throws IOException 
	 */
	@Override
	public void exec(String[] args,int type, PersonnelAdministratif personnelAdministratif, Etablissement etablissement, List<Message> messages) throws SQLException, IOException {
		boolean cmdToDisplayImprimer = getParameter("imprimer").isPresent(args);
		
		
		if (cmdToDisplayImprimer) {
			personnelAdministratif.imprimerBulletin(etablissement);
			return;
		}
		else {
			System.out.println("La commande 'imprimer' est mal ecrite");
		}		
	}
	
	/**
	 * Executable pour un utilisateur
	 */
	@Override
	public Utilisateur execint(String[] args, Etablissement etablissement) throws SQLException {
		return null;
	}

}
