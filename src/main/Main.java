package main;

/**
 * Importations
 */
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

import bdd.CreationEtablissement;
import bdd.Etablissement;
import communication.ListMessage;
import communication.Message;
import main.command.Commands;
import main.command.model.Command;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;
import users.Utilisateur;

/**
 * Classe Main permettant d'executer le programme 
 * @author Bouillet Ducatillion
 */
public class Main {
	
	/**
	 * Attributs 
	 */
	private static Scanner scanner;
	private static Connection connection;

	/**
	 * Main du programme 
	 * @param args
	 * @throws SQLException
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws SQLException, ParseException, IOException {
		
		System.out.println("======================================================");
		System.out.println("Chargement en cours....");
		Pomelo.getInstance();

		scanner = new Scanner(System.in);
		connection = SingletonConnection.getInstance();
		int type =0;
		Utilisateur personne = null;
		Etudiant etudiant = null;
		Professeur professeur = null;
		PersonnelAdministratif personnelAdministratif = null;
		
		Etablissement etab = CreationEtablissement.creationEtablissement();
		Etablissement eta = CreationEtablissement.creationEtablissement2(etab);
		
		List<Message> messages = ListMessage.creationListMessagesBDD(eta);
		
		System.out.println("Chargement termine!");
		
		while (type == 0) {
			System.err.print("Pomelo$> ");
			System.out.println("Veuillez vous connecter, de cette facon : \n login -u=PNom -pwd=NPrenomId");

			String commandLineFirst = scanner.nextLine();
			Command commandFirst = Commands.find(commandLineFirst, type);
			personne = commandFirst.execint(commandLineFirst.split(" "),eta);
			type = personne.getType();
			if (type == 1){
				professeur = (Professeur)personne;
			}
			if (type == 2){
				etudiant = (Etudiant)personne;
			}
			if (type == 3){
				personnelAdministratif = (PersonnelAdministratif)personne;
			}
		}
		System.out.println(getWelcomeMessage(type));
		System.err.print("Pomelo$> ");
		
		while (scanner.hasNext()) {
			String commandLine = scanner.nextLine();
			if (commandLine.equals("deconnection")) {
				break;
			}
			Command command = Commands.find(commandLine, type);
			if (command != null && type == 1) {
				command.exec(commandLine.split(" "),type,professeur, eta, messages);
			}
			else if (command != null && type == 2) {
				command.exec(commandLine.split(" "),type,etudiant, eta, messages);
			}
			else if (command != null && type == 3) {
				command.exec(commandLine.split(" "),type,personnelAdministratif, eta, messages);
			}
			else {
				if (commandLine.equals("")) {
					continue;
				}
				else {
					System.out.println("Commande inconnue");
				}
			}
			System.err.print("Pomelo$> ");
		}

		System.out.println("Vous etes deconnecte");
		scanner.close();
		connection.close();
	}
	
	/**
	 * Getter du scanner
	 * @return scanner
	 */
	public static Scanner getScanner() {
		return scanner;
	}
	
	/**
	 * Getteur de l'attribut connection 
	 * @return connection
	 */
	public static Connection getConnection() {
		return connection;
	}
	
	/**
	 * Methode permettant de creer le message de bienvenue
	 * @return chaine de caracteres du message de bienvenue
	 */
	private static String getWelcomeMessage(int type) {
		String str = "======================================================\n"
				+ "Bienvenue dans l'application Pomelo! \n"
				+ "Vous pouvez realiser les commandes suivantes :\n";
		for (Command command : Commands.getRegisterCommands(type)) {
			str += "  -" + command.toString() + "\n";
		}
		str += "======================================================";

		return str;
	}
}
