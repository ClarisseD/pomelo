package fiche;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;
import java.text.Format;
import java.text.SimpleDateFormat;

import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;

/**
 * Classe FicheInformation
 * @author Bouillet Ducatillion
 *
 */
public class FicheInformation {
	
	/**
	 * Attribut
	 */
	private List<String> attributs;
	
	/**
	 * Getter et setter attributs
	 * @return la liste des attributs de la personne concernee par la fiche
	 */
	public List<String> getAttributs() {
		return attributs;
	}
	public void setAttributs(List<String> attributs) {
		this.attributs = attributs;
	}
	
	/**
	 * Constructeur de la fiche d'un etudiant
	 * @param etudiant
	 */
	public FicheInformation(Etudiant etudiant) {
		this.attributs = new ArrayList<String>();
		attributs.add("2");
		attributs.add(etudiant.getNomUtilisateur());
		attributs.add(etudiant.getMotDePasse());
		attributs.add(etudiant.getNom());
		attributs.add(etudiant.getPrenom());
		attributs.add(etudiant.getPromotionString()); 
		attributs.add(etudiant.getAdresse());
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String s = formatter.format(etudiant.getDateNaissance());
		attributs.add(s);
	}
	
	/**
	 * Constructeur de la fiche d'un professeur
	 * @param professeur
	 */
	public FicheInformation(Professeur professeur) {
		this.attributs = new ArrayList<String>();
		String matiere = "" + professeur.getMatiere() + "";
		attributs.add("1");
		attributs.add(professeur.getNomUtilisateur());
		attributs.add(professeur.getMotDePasse());
		attributs.add(professeur.getNom());
		attributs.add(professeur.getPrenom());
		attributs.add(matiere);
		attributs.add(professeur.getAdresse());
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String s = formatter.format(professeur.getDateNaissance());
		attributs.add(s);
	}
	
	/**
	 * Constructeur de la fiche d'un personnel administratif
	 * @param persAdmin
	 */
	public FicheInformation(PersonnelAdministratif persAdmin) {
		this.attributs = new ArrayList<String>();
		String fonction = "" + persAdmin.getFonction() + "";
		attributs.add("3");
		attributs.add(persAdmin.getNomUtilisateur());
		attributs.add(persAdmin.getMotDePasse());
		attributs.add(persAdmin.getNom());
		attributs.add(persAdmin.getPrenom());
		attributs.add(fonction);
		attributs.add(persAdmin.getAdresse());
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String s = formatter.format(persAdmin.getDateNaissance());
		attributs.add(s);
	}
	
	/**
	 * Surcharge de la methode toString pour la visualisation des fiches
	 * permet de lister les attributs de la personne concernee par la fiche
	 */
	@Override
	public String toString() {
		if (attributs.get(0).equals("1")) {
			return ("nomUtilisareur: " + attributs.get(1) + "\n motDePasse: " + attributs.get(2) + "\n nom: " + attributs.get(3) +
					"\n prenom: " + attributs.get(4) + "\n matiere: " + attributs.get(5) + "\n adresse: " + attributs.get(6) + 
					"\n dateNaissance: " + attributs.get(7));
		}
		else if(attributs.get(0).equals("3")) {
			return ("nomUtilisareur: " + attributs.get(1) + "\n motDePasse: " + attributs.get(2) + "\n nom: " + attributs.get(3) +
					"\n prenom: " + attributs.get(4) + "\n fonction: " + attributs.get(5) + "\n adresse: " + attributs.get(6) +
					"\n dateNaissance: " + attributs.get(7));
		}
		else {
			return ("nomUtilisareur: " + attributs.get(1) + "\n motDePasse: " + attributs.get(2) + "\n nom: " + attributs.get(3) + 
					"\n prenom: " + attributs.get(4) + "\n promotion: " + attributs.get(5) + "\n adresse: " + attributs.get(6) +
					"\n dateNaissance: " + attributs.get(7));
		}
	}
}
