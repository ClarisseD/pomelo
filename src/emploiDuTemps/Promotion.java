package emploiDuTemps;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;

import bdd.Etablissement;
import users.Etudiant;

/**
 * Classe Promotion
 * @author Bouillet Ducatillion
 *
 */
public class Promotion {
	
	/**
	 * Attributs
	 */
	public String nom;
	public int nbEtudiant;
	public List<Etudiant> etudiants;
	public EmploiDuTemps edt;
	
	/**
	 * Constructeur de Promotion
	 * @param nom
	 * @param nbEtudiant
	 * @param edt
	 */
	public Promotion(String nom, int nbEtudiant, EmploiDuTemps edt) {
		this.nom = nom;
		this.nbEtudiant = nbEtudiant;
		this.etudiants = new ArrayList<Etudiant>();
		this.edt = edt;
	}

	/**
	 * Getter nom
	 * @return le nom de la promotion
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Permet de convertir une chaine de caractere en la promotion qu'elle designe
	 * @param nom
	 * @param etablissement
	 * @return
	 */
	public static Promotion get(String nom, Etablissement etablissement) {
		for (Promotion promotion : etablissement.getPromotions()) {
			nom = nom.replaceAll("\\s", "");
			if (promotion.getNom().equals(nom)) {
				return promotion;
			}
		}
		return null;
	}
	
	/**
	 * Methode permettant d'afficher le nom de la promotion en String
	 */
	public String toString() {
		return this.nom;
	}
		
}
