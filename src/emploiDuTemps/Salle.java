package emploiDuTemps;

/**
 * Importations
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.Main;

/**
 * Classe Salle
 * @author Bouillet Ducatillion
 *
 */
public class Salle {

	/**
	 * Attributs
	 */
	public int nbPlaces;
	public int numeroSalle;
	public String type;
	public String equipement;
	public boolean etatSalle;

	/**
	 * Constructeur de Salle
	 * @param nbPlaces2 
	 * @param nbPlace
	 * @param type
	 * @param etatSalle
	 */
	public Salle(int numero, int nbPlaces, String type, boolean etatSalle, String equipement) {
		this.numeroSalle =numero;
		this.nbPlaces =nbPlaces;
		this.type = type;
		this.equipement = equipement;
		this.etatSalle = etatSalle;
	}

	/**
	 * Getter et Setter du nombre de places de la salle
	 * @return nbPlaces
	 */
	public int getNbPlaces() {
		return this.nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	
	/**
	 * Getter du numero de la salle
	 * @return le numero de la salle
	 */
	public int getNumeroSalle() {
		return this.numeroSalle;
	}

	/**
	 * Getter et Setter de l'equipement de la salle
	 * @return equipement
	 */
	public String getEquipement() {
		return this.equipement;
	}
	public void setEquipement(String equipement) {
		this.equipement = equipement;
	}

	/**
	 * Methode permettant de creer une salle soit avec un equipement d'origine soit en le modifiant
	 * @param origin
	 * @param numeroSalle
	 * @return salle creee
	 */
	public static Salle creationSalle(boolean origin, int numeroSalle) {
		Salle salle = null;
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("SELECT * FROM salle WHERE id = ?");
			statement.setInt(1, numeroSalle);
			ResultSet resultat = statement.executeQuery();
			if (resultat.next()) {
				int nbPlaces = resultat.getInt(2);
				String type = resultat.getString(3);
				boolean etatSalle = resultat.getBoolean(4);
				String equipement = resultat.getString(5);
				salle = new Salle(numeroSalle, nbPlaces, type, etatSalle, equipement);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		if (!origin) {
			System.out.print("Voulez vous modifier la salle? \n -Oui \n -Non");
			String cmd = Main.getScanner().nextLine();
			cmd = cmd.toLowerCase();
			if(cmd.equals("oui")) {
				System.out.println("Voulez vous modifier : \n -capacite \n -equipement");
				String cmd2 = Main.getScanner().nextLine();
				cmd2 = cmd2.toLowerCase();
				if (cmd2.equals("capacite")) {
					System.out.println("Ancienne capacite : " + salle.getNbPlaces());
					System.out.println("Nouvelle capacite : ");
					int cmd3 = Main.getScanner().nextInt();
					salle.setNbPlaces(cmd3);
				}
				else if (cmd2.equals("equipement")) {
					System.out.println("Anciens equipements : " + salle.getEquipement());
					while(true) {
						System.out.println("Entrez soit une liste d'equipements separes par une virgule, soit exit");
						String cmd4 = Main.getScanner().nextLine();
						cmd4 = cmd4.toLowerCase();
						if (cmd4.equals("exit")) {
							break;
						}
						else {
							salle.setEquipement(cmd4);
						}
					}
				}
				return salle;
			}
			else if(cmd.equals("non")) {
				return salle;
			}
		}
		return salle;
	}
	
}
