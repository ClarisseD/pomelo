package emploiDuTemps;

/**
 * Importation
 */

import bdd.Matiere;
import users.Professeur;

/**
 * Classe Cours
 * @author Bouillet Ducatillion
 *
 */
public class Cours {
	
	/**
	 * Attributs
	 */
	public Matiere matiere;
	public Professeur professeur;
	public Salle salle;
	public String horaireDebut;
	public String horaireFin;
	public String date;
	public Promotion promotion;
	
	/**
	 * Constructeur de Cours
	 * @param matiere
	 * @param professeur
	 * @param salle
	 * @param horaireDebut
	 * @param horaireFin
	 */
	public Cours(Matiere matiere, Professeur professeur, Promotion promotion, 
			Salle salle, String horaireDebut, String horaireFin, String date) {
		this.matiere = matiere;
		this.professeur = professeur;
		this.promotion = promotion;
		this.salle = salle;
		this.horaireDebut = horaireDebut;
		this.horaireFin = horaireFin;
		this.date = date;
	}
	
	/**
	 * Methode toString permettant d'afficher les caracteristiques d'un cours
	 */
	@Override
	public String toString() {
		return " Matiere: " + this.matiere + "\n Professeur: " + this.professeur.getNom() + " " + this.professeur.getPrenom() + 
				"\n Promotion: " + this.promotion + "\n Salle: " + this.salle.getNumeroSalle() + 
				"\n Le " + this.date +
				"\n Horaire: De " + this.horaireDebut + " a " + this.horaireFin + "\n";
	}
	
	/**
	 * Getteur de matiere
	 * @return matiere
	 */
	public Matiere getMatiere() {
		return matiere;
	}
	
	/**
	 * Getter de professeur
	 * @return le professeur qui donne le cours
	 */
	public Professeur getProf() {
		return this.professeur;
	}
	
	/**
	 * Getter de salle
	 * @return la salle dans laquelle se deroulera le cours
	 */
	public Salle getSalle() {
		return this.salle;
	}
	
	/**
	 * Getter de horaireDebut
	 * @return l'horaire de debut du cours
	 */
	public String getHoraireDebut() {
		return this.horaireDebut;
	}
	
	/**
	 * Getter de horaireFin
	 * @return l'horaire de fin du cours
	 */
	public String getHoraireFin() {
		return this.horaireFin;
	}
	
	/**
	 * Getteur de date
	 * @return date du cours
	 */
	public String getDate(){
		return this.date;
	}
	
	/**
	 * Getter de promotion
	 * @return la promotion qui assistera au cours
	 */
	public Promotion getPromo() {
		return this.promotion;
	}
	
}
