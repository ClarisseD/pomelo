package users;

/**
 * Importations
 */
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.PreparedStatement;
import bdd.Etablissement;
import bdd.Matiere;
import bulletin.Bulletin;
import emploiDuTemps.Cours;
import emploiDuTemps.CreationEmploiDuTemps;
import emploiDuTemps.EmploiDuTemps;
import emploiDuTemps.Promotion;
import emploiDuTemps.Salle;
import fiche.FicheInformation;
import main.Main;

/**
 * Classe PersonnelAdministratif qui herite de Utilisateur
 * @author Bouillet Ducatillion
 *
 */
public class PersonnelAdministratif extends Utilisateur{
	/**
	 * Attributs
	 */
	private String fonction;

	/**
	 * Getter fonction
	 * @return la fonction du personnel administratif
	 */
	public String getFonction() {
		return fonction;
	}

	/**
	 * Getter du type
	 * @return le type correspondant a un personnel administratif
	 */
	public int getType(){
		return 3;
	}

	/**
	 * Constructeur avec tous les atributs de PersonnelAdministratif
	 * @param nom
	 * @param prenom
	 * @param fonction
	 * @param adresse
	 * @param dateNaissance
	 * @param nomUtilisateur
	 * @param motDePasse
	 */
	public PersonnelAdministratif(String nomUtilisateur, String motDePasse, String nom, String prenom, String fonction, 
			String adresse, Date dateNaissance) {
		super(nomUtilisateur, motDePasse, nom, prenom, adresse, dateNaissance);
		this.fonction = fonction;
	}

	/**
	 * Constructeur avec les atributs nom, prenom, fonction, adresse et dateNaissance de PersonnelAdministratif
	 * @param nom
	 * @param prenom
	 * @param fonction
	 * @param adresse
	 * @param dateNaissance
	 */
	public PersonnelAdministratif(String nom, String prenom, String fonction, 
			String adresse, Date dateNaissance) {
		super(nom, prenom, adresse, dateNaissance);
		this.fonction = fonction;
	}

	/**
	 * Methode visualiserFiche permettant de visualiser sa fiche personnelle en tant que personnel administratif
	 * @param persAdmin
	 * @return String, liste des attributs de la personne
	 */
	public String visualiserFiche(PersonnelAdministratif persAdmin) {
		FicheInformation fI = new FicheInformation(persAdmin);
		return fI.toString();
	}

	/**
	 * Methode imprimierBulletin permet de retourner le bulletin a imprimer
	 * @param etudiant
	 * @return le bulletin a imprimer
	 * @throws IOException 
	 */
	public void imprimerBulletin(Etablissement etablissement) throws IOException {

		CreationPersonne etu = new CreationPersonne();
		Etudiant etudiant = etu.creationEtudiantNP(etablissement);
		etudiant = Bulletin.creationBulletin(etudiant);
		List<String> contenuBulletin = etudiant.visualiserBulletin();
		
		System.out.println("\nVeuillez entrer le chemin du dossier ou sera enregistrer le bulletin");
		String cheminabs = Main.getScanner().nextLine();
		Path logFile = Paths.get(cheminabs);
		if(!Files.exists(logFile)) {
			try {
				Files.createFile(logFile);
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		try(BufferedWriter writer = Files.newBufferedWriter(logFile, StandardCharsets.UTF_8, StandardOpenOption.WRITE)){
			writer.write("Voici le bulletin de: \n \n");
			writer.write(etudiant.getNom() + "\n" + etudiant.getPrenom() + "\n" + etudiant.getDateNaissance() +
					"\n" + etudiant.getAdresse() + "\n" + etudiant.getPromotion().getNom() + "\n \n");
			for(String premierePartie : contenuBulletin) {
				writer.write(premierePartie);
			}
		}catch(IOException e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Methode pour saisir et ajouter un cours dans l'emploi du temps du professeur et de la promotion concernes
	 * @param etablissement
	 * @throws ParseException
	 */
	public Cours saisieCours(Etablissement etablissement) throws ParseException {
		System.out.println(" Veuiller saisir le numero du professeur voulu dans la liste suivante :");
		int i = 0;
		for (Professeur professeur : etablissement.getProfesseurs()) {
			System.out.println(i + ": " + professeur.getNom() + " " + professeur.getPrenom() + "\n");
			i++;
		}
		int a = Main.getScanner().nextInt();
		Professeur professeur = etablissement.getProfesseurs().get(a);
		Matiere matiere = professeur.getMatiere();

		System.out.println(" Veuillez saisir le numero de la salle dans la liste suivante :");
		int j = 0;
		for (Salle salle : etablissement.getSalles()) {
			System.out.println(j + ": " + salle.getNumeroSalle());
			j++;
		}
		int b = Main.getScanner().nextInt();
		Main.getScanner().nextLine();
		Salle salle = etablissement.getSalles().get(b);

		System.out.println(" Veuillez saisir le numero de la promotion voulue dans la liste suivante");
		int k = 0;
		for (Promotion promotion : etablissement.getPromotions()) {
			System.out.println(k + ": " + promotion.getNom());
			k++;			
		}
		int c = Main.getScanner().nextInt();
		Main.getScanner().nextLine();
		Promotion promotion = etablissement.getPromotions().get(c);

		System.out.println(" Veuillez saisir l'heure de debut: ");
		String heureDebut = Main.getScanner().nextLine();

		System.out.println(" Veuillez saisir l'heure de fin: ");
		String heureFin = Main.getScanner().nextLine();

		System.out.println(" Veuillez saisir la date (YYYY-MM-DD): ");
		String date = Main.getScanner().nextLine();

		Cours cours = new Cours(matiere, professeur, promotion, salle, heureDebut, heureFin, date);
		return cours;
	}

	/**
	 * Methode pour ajouter un cours deja cree dans l'emploi du temps de la promotion et du proffesseur concernes
	 * @param cours
	 */
	public void ajoutCoursEDT(Cours cours) {
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("INSERT INTO emploidutemps VALUES (?, "
					+ "?, ?, ?, ?, ?, ?)");
			statement.setString(1,"" + cours.getMatiere() + "");
			statement.setString(2, cours.getProf().getNom());
			statement.setInt(3, cours.getSalle().getNumeroSalle());
			statement.setString(4, cours.getPromo().getNom());
			statement.setString(5, cours.getDate());
			statement.setString(6, cours.getHoraireDebut());
			statement.setString(7, cours.getHoraireFin());
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode pour supprimer un cours des emplois du temps du professeur et de la promotion concernes
	 * @param cours
	 */
	public void suppressionCoursEDT(Cours cours) {
		try {
			PreparedStatement statement = Main.getConnection().prepareStatement("DELETE FROM emploidutemps WHERE matiere = ? AND "
					+ "professeur = ? AND salle = ? AND promotion = ? AND horairedebut = ? AND horairefin = ? AND date = ?");
			statement.setString(1,"" + cours.getMatiere() + "");
			statement.setString(2, cours.getProf().getNom());
			statement.setInt(3, cours.getSalle().getNumeroSalle());
			statement.setString(4, cours.getPromo().getNom());
			statement.setString(5, cours.getHoraireDebut());
			statement.setString(6, cours.getHoraireFin());
			statement.setString(7, cours.getDate());
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode pour modifier un cours de l'emploi du temps
	 * @param etablissement
	 * @param cours
	 * @throws ParseException
	 */
	public void majEDT(Etablissement etablissement, EmploiDuTemps emploiDuTemps) throws ParseException {

		if (emploiDuTemps.getEdt().get(0).getPromo().getNom().equals("ING1")) {
			etablissement.getEtudiants().get(0).visualiserEdT(emploiDuTemps);
		}
		else {
			etablissement.getEtudiants().get(6).visualiserEdT(emploiDuTemps);
		}
		System.out.println("Quel cours voulez vous modifier? Entrez l'indice du cours");
		int idCours = Main.getScanner().nextInt();
		Main.getScanner().nextLine();
		Cours cours = emploiDuTemps.getEdt().get(idCours);
		System.out.println("Que voulez-vous modifier? \n -professeur \n -salle \n -promotion \n -date \n -horaireDebut \n -horaireFin");
		String aModifier = Main.getScanner().nextLine();
		if (aModifier.equals("professeur")) {
			System.out.println("Veuiller saisir le numero du professeur voulu dans la liste suivante :");
			int h = 0;
			for (Professeur professeur : etablissement.getProfesseurs()) {
				System.out.println(h + ": " + professeur.getNom() + " " + professeur.getPrenom() + "\n");
				h++;
			}
			int r = Main.getScanner().nextInt();
			Main.getScanner().nextLine();
			Professeur professeur = etablissement.getProfesseurs().get(r);
			Matiere matiere = professeur.getMatiere();
			Cours newcours = new Cours(matiere, professeur, cours.getPromo(), cours.getSalle(), cours.getHoraireDebut(), cours.getHoraireFin(), cours.getDate());
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}
		else if (aModifier.equals("salle")) {
			System.out.println("Veuillez saisir le numero de la salle dans la liste suivante :");
			int j = 0;
			for (Salle salle : etablissement.getSalles()) {
				System.out.println(j + ": " + salle.getNumeroSalle());
				j++;
			}
			int b = Main.getScanner().nextInt();
			Salle salle = etablissement.getSalles().get(b);
			Cours newcours = new Cours(cours.getProf().getMatiere(), cours.getProf(), cours.getPromo(), salle, cours.getHoraireDebut(), cours.getHoraireFin(), cours.getDate());
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}
		else if (aModifier.equals("promotion")) {
			System.out.println("Veuillez saisir le numero de la promotion voulue dans la liste suivante");
			int k = 0;
			for (Promotion promotion : etablissement.getPromotions()) {
				System.out.println(k + ": " + promotion.getNom());
				k++;			
			}
			int c = Main.getScanner().nextInt();
			Promotion promotion = etablissement.getPromotions().get(c);
			Cours newcours = new Cours(cours.getProf().getMatiere(), cours.getProf(), promotion, cours.getSalle(), cours.getHoraireDebut(), cours.getHoraireFin(), cours.getDate());
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}
		else if (aModifier.equals("date")) {
			System.out.println("Veuillez saisir la date du cours (YYYY-MM-DD)");
			String date = Main.getScanner().nextLine();
			Cours newcours = new Cours(cours.getProf().getMatiere(), cours.getProf(), cours.getPromo(), cours.getSalle(), cours.getHoraireDebut(), cours.getHoraireFin(), date);
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}
		else if (aModifier.equals("horaireDebut")) {
			System.out.println("Veuillez saisir l'heure de debut du cours");
			String heureDebut = Main.getScanner().nextLine();
			Cours newcours = new Cours(cours.getProf().getMatiere(), cours.getProf(), cours.getPromo(), cours.getSalle(), heureDebut, cours.getHoraireFin(), cours.getDate());
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}
		else if (aModifier.equals("horairefin")) {
			System.out.println("Veuillez saisir l'heure de debut du cours");
			String heureFin = Main.getScanner().nextLine();
			Cours newcours = new Cours(cours.getProf().getMatiere(), cours.getProf(), cours.getPromo(), cours.getSalle(), cours.getHoraireDebut(), heureFin, cours.getDate());
			ajoutCoursEDT(newcours);
			suppressionCoursEDT(cours);
		}

	}
	
	/**
	 * Methode permettant de choisir une promotion afin d'en creer l'emploi du temps
	 * @param eta
	 * @return emploi du temps de la promotion
	 */
	public EmploiDuTemps choixPromotionCreation(Etablissement eta) {
		System.out.println("De quelle promotion voulez vous modifier un cours? \n -ING1 \n -ING2");
		String promo = Main.getScanner().nextLine();

		EmploiDuTemps emploiDuTemps = CreationEmploiDuTemps.creationEmploiDuTemps(promo, eta);

		return emploiDuTemps;
	}

	/**
	 * Methode permettant de gerer les salles en modifiant les attributs d'une salle
	 * @param numeroSalle
	 * @param modif
	 * @param eta
	 */
	public void gestionSalle(int numeroSalle, int modif, Etablissement eta) {
		if (modif == 1) {
			try {
				System.out.println("Entrez la nouvelle capacite :");
				int capacite = Main.getScanner().nextInt();
				PreparedStatement statement = Main.getConnection().prepareStatement("UPDATE salle SET capacite = ? WHERE id = ?");
				statement.setInt(1, capacite);
				statement.setInt(2, numeroSalle);
				statement.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (modif == 2) {
			try {
				System.out.println("Entrez le nouveau type :");
				String type = Main.getScanner().nextLine();
				PreparedStatement statement = Main.getConnection().prepareStatement("UPDATE salle SET type = ? WHERE id = ?");
				statement.setString(1, type);
				statement.setInt(2, numeroSalle);
				statement.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (modif == 3) {
			try {
				System.out.println("Entrez le nouvel etat :");
				boolean etat = Main.getScanner().nextBoolean();
				PreparedStatement statement = Main.getConnection().prepareStatement("UPDATE salle SET etat = ? WHERE id = ?");
				statement.setBoolean(1, etat);
				statement.setInt(2, numeroSalle);
				statement.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (modif == 4) {
			try {
				System.out.println("Entrez les nouveaux equipements (sur la meme ligne separes par une virgule) :");
				String equipement = Main.getScanner().nextLine();
				PreparedStatement statement = Main.getConnection().prepareStatement("UPDATE salle SET equipement = ? WHERE id = ?");
				statement.setString(1, equipement);
				statement.setInt(2, numeroSalle);
				statement.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}













