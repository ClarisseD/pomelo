package bdd;

/**
 * Importations
 */
import java.util.ArrayList;
import java.util.List;
import emploiDuTemps.Promotion;
import emploiDuTemps.Salle;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;

/**
 * Classe Etablissement
 * @author Bouillet Ducatillion
 *
 */
public class Etablissement {
	
	/**
	 * Attributs
	 */
	public List<Etudiant> etudiants;
	public List<Professeur> professeurs;
	public List<PersonnelAdministratif> personnelsAdministratif;
	public List<Salle> salles;
	public List<Promotion> promotions;
	
	/**
	 * Constructeur d'Etablissement
	 */
	public Etablissement() {
		this.etudiants = new ArrayList<Etudiant>();
		this.professeurs = new ArrayList<Professeur>();
		this.personnelsAdministratif = new ArrayList<PersonnelAdministratif>();
		this.salles = new ArrayList<Salle>();
		this.promotions = new ArrayList<Promotion>();
	}
	
	/**
	 * Getter de etudiants
	 * @return la liste des etudiants de l'etablissement
	 */
	public List<Etudiant> getEtudiants() {
		return this.etudiants;
	}
	
	/**
	 * Getter professeurs
	 * @return la liste des professeurs de l'etablissement
	 */
	public List<Professeur> getProfesseurs() {
		return this.professeurs;
	}
	
	/**
	 * Getter de personnelAdministratif
	 * @return la liste du personnel administraif de l'etablissement
	 */
	public List<PersonnelAdministratif> getPersonnelAdministratif() {
		return this.personnelsAdministratif;
	}
	
	/**
	 * Getter de salles
	 * @return la liste des salles de l'etablissement
	 */
	public List<Salle> getSalles() {
		return this.salles;
	}
	
	/**
	 * Getter de promotions
	 * @return la liste des promotions de l'etablissement
	 */
	public List<Promotion> getPromotions() {
		return this.promotions;
	}	
}
