package bdd;

/**
 * Enumeration des matieres dans l'etablissement
 * @author Bouillet Ducatillion
 *
 */
public enum Matiere {
	Topometrie,
	Photogrammetrie,
	Informatique,
	Teledetection,
	Mathematiques,
	Anglais,
	LV2,
	Geodesie,
}
