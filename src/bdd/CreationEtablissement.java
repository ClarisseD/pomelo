package bdd;

/**
 * Importations
 */
import emploiDuTemps.EmploiDuTemps;
import emploiDuTemps.Promotion;
import emploiDuTemps.Salle;
import users.CreationPersonne;
import users.Etudiant;
import users.PersonnelAdministratif;
import users.Professeur;

/**
 * Classe CreationEtablissement 
 * @author Bouillet Ducatillion
 *
 */
public class CreationEtablissement {
	
	/**
	 * Methode permettant de remplir les listes d'attributs (salles et promotions) de l'etablissement
	 * @return l'etablissement avec ses listes d'attributs (salles et promotions) remplies
	 */
	public static Etablissement creationEtablissement() {
		
		Etablissement etablissement = new Etablissement();
		
		for (int i =1 ; i<8; i++){
			Salle salle = Salle.creationSalle(true, i);
			etablissement.getSalles().add(salle);
		}
				
		EmploiDuTemps edtING1 = null;
		Promotion ing1 = new Promotion("ING1", 5, edtING1);
		etablissement.getPromotions().add(ing1);
		
		EmploiDuTemps edtING2 = null;
		Promotion ing2 = new Promotion("ING2", 5, edtING2);
		etablissement.getPromotions().add(ing2);
			
		
		return etablissement;
		
	}
	
	/**
	 * Methode permettant de remplir les listes restantes de l'etablissement qui avait deja ete cree avant 
	 * @param etablissement
	 * @return etablissement avec toutes les listes d'attributs
	 */
	public static Etablissement creationEtablissement2(Etablissement etablissement){
		
		CreationPersonne personneCreeFT = new CreationPersonne("FTertre", "TFranck1");
		Professeur professeurFT = personneCreeFT.creationProfesseur();
		etablissement.getProfesseurs().add(professeurFT);
		
		CreationPersonne personneCreeAP = new CreationPersonne("APinte", "PAntoine2");
		Professeur professeurAP = personneCreeAP.creationProfesseur();
		etablissement.getProfesseurs().add(professeurAP);
		
		CreationPersonne personneCreeCB = new CreationPersonne("CBouche", "BClement3");
		Professeur professeurCB = personneCreeCB.creationProfesseur();
		etablissement.getProfesseurs().add(professeurCB);
		
		CreationPersonne personneCreeMP = new CreationPersonne("MPoupee", "PMarc4");
		Professeur professeurMP = personneCreeMP.creationProfesseur();
		etablissement.getProfesseurs().add(professeurMP);
		
		CreationPersonne personneCreeAC = new CreationPersonne("AChevreuil", "CAntoine5");
		Professeur professeurAC = personneCreeAC.creationProfesseur();
		etablissement.getProfesseurs().add(professeurAC);
		
		CreationPersonne personneCreeSJ = new CreationPersonne("SJouffroy", "JStephane6");
		Professeur professeurSJ = personneCreeSJ.creationProfesseur();
		etablissement.getProfesseurs().add(professeurSJ);
		
		CreationPersonne personneCreeGRH = new CreationPersonne("GRiveraHidalgo", "RHGloria7");
		Professeur professeurGRH = personneCreeGRH.creationProfesseur();
		etablissement.getProfesseurs().add(professeurGRH);
		
		CreationPersonne personneCreeSB = new CreationPersonne("SBotton", "BSerge8");
		Professeur professeurSB = personneCreeSB.creationProfesseur();
		etablissement.getProfesseurs().add(professeurSB);
		
		CreationPersonne personneCreeMLB = new CreationPersonne("MLBouillet", "BMarieLaure1");
		Etudiant etudiantMLB = personneCreeMLB.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantMLB);
		
		CreationPersonne personneCreeCD = new CreationPersonne("CDucatillion", "DClarisse2");
		Etudiant etudiantCD = personneCreeCD.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantCD);
		
		CreationPersonne personneCreeFP = new CreationPersonne("FPrevost", "PFranck3");
		Etudiant etudiantFP = personneCreeFP.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantFP);
		
		CreationPersonne personneCreeWS = new CreationPersonne("WSaurin", "SWilliam4");
		Etudiant etudiantWS = personneCreeWS.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantWS);
		
		CreationPersonne personneCreeIB = new CreationPersonne("IBoulanger", "BIsabelle5");
		Etudiant etudiantIB = personneCreeIB.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantIB);
		
		CreationPersonne personneCreeLD = new CreationPersonne("LDupond", "DLouis6");
		Etudiant etudiantLD = personneCreeLD.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantLD);
		
		CreationPersonne personneCreeAL = new CreationPersonne("ALavigne", "LAudrey7");
		Etudiant etudiantAL = personneCreeAL.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantAL);
		
		CreationPersonne personneCreeALb = new CreationPersonne("ALabel", "LAgathe8");
		Etudiant etudiantALb = personneCreeALb.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantALb);
		
		CreationPersonne personneCreeMJ = new CreationPersonne("MJacques", "JMarius9");
		Etudiant etudiantMJ = personneCreeMJ.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantMJ);
		
		CreationPersonne personneCreeGL = new CreationPersonne("GLancien", "LGuillaume10");
		Etudiant etudiantGL = personneCreeGL.creationEtudiant(etablissement);
		etablissement.getEtudiants().add(etudiantGL);
		
		CreationPersonne personneCreeMD = new CreationPersonne("MDupont", "DMichel1");
		PersonnelAdministratif persadminMD = personneCreeMD.creationPersonnelAdministratif();
		etablissement.getPersonnelAdministratif().add(persadminMD);
		
		CreationPersonne personneCreeME = new CreationPersonne("MEvian", "EMarie2");
		PersonnelAdministratif persadminME = personneCreeME.creationPersonnelAdministratif();
		etablissement.getPersonnelAdministratif().add(persadminME);
		
		return etablissement;

	}

}
